﻿using System;
using System.Collections;

namespace Iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            Collection col = new Collection();
            col[0] = new Person("Marcin", "Murawski");
            col[1] = new Person("Andrew", "Ryan");
            col[2] = new Person("Władysław", "Anders");
            col[3] = new Person("Piotr", "Jarzebiewski");
            col[4] = new Person("John", "Shepard");
            col[5] = new Person("Booker", "DeWitt");
            col[6] = new Person("Gerwant", "Riviva");
            col[7] = new Person("Stephen", "Strange");
            col[8] = new Person("Ryszard", "Szymanski");


            Iterator iter = col.CreateITER();

            iter.Step = 1;

            for (Person p = iter.first(); !iter.IsFullLoop; p = iter.next())
            {
                Console.WriteLine(p.Name + " " + p.Surname);
            }
        }
    }

    interface IAColl
    {
        Iterator CreateITER();
    }

     interface IAIter
    {
        Person first();
        Person next();

        bool IsFullLoop { get; }

        Person currentP { get; }
    }

    class Person
    {
        private string _name;
        private string _surname;

        public Person(string name, string surname)
        {
            this._name = name;
            this._surname = surname;
        }

        public string Name
        {
            get { return _name; }
        }
        public string Surname
        {
            get { return _surname; }
        }
    }

    class Collection : IAColl
    {
        private ArrayList _people = new ArrayList();

        public Iterator CreateITER()
        {
            return new Iterator(this);
        }

        public int Count
        {
            get { return _people.Count; }
        }

        public object this[int index]
        {
            get { return _people[index]; }
            set { _people.Add(value); }
        }
    }

    class Iterator : IAIter
    {
        private Collection _collection;
        private int _curr;
        private int _step = 1;

        public Iterator(Collection collection)
        {
            this._collection = collection;
        }

        public Person first()
        {
            _curr = 0;
            return _collection[_curr] as Person;
        }

        public bool IsFullLoop
        {
            get { return _curr >= _collection.Count; }
        }

        public Person next()
        {
            _curr += _step;
            if (!IsFullLoop)
            {
                return _collection[_curr] as Person;
            }
            else
            { return null; }
        }

        public int Step
        {
            get { return _step; }
            set { _step = value;}
        }

        public Person currentP
        {
            get { return _collection[_curr] as Person; }
        }
    }
}
